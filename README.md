Projeto desenvolvido durante o curso de HTML5 e CSS3: módulo 1 do **Curso em Vídeo** (https://www.cursoemvideo.com/course/html5-css3-modulo1/)

**Nos exercícos foram aplicados os seguintes conceitos:**

* Parágrafos e quebras;
* Símbolos e Emojis;
* Direitos autorais de mídias;
* Tag img;
* Favicon;
* Hierarquia de títulos;
* Semântica no HTML5;
* Citações e códigos;
* Listas OL e UL;
* Links e âncoras;
* Links internos;
* Incorporação de vídeos externos;
* Estilos CSS internos, inline e externos.

**Ferramentas utilizadas:**

* Visual Studio Code Versão 1.54.1;
* Navegador Google Chrome Versão 89.0.4389.82;
* Windows 10 Versão 1909 (OS Build 18363.1379);

**Para visualizar as funcionalidades dos exercícios, baixe todos os arquivos em uma mesma pasta e abra o arquivo index.html de cada exercício no seu navegador.**
 
